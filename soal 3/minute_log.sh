mkdir -p ~/log

TIMESTAMP=`date "+%Y%m%d%H%M%S"`
TIMESTAMP1=`date "+%Y%m%d%H"`
infRam=$(free -m | awk 'BEGIN{OFS=","} NR!=1 {
i=2
while (i <= NF) {
	printf $i","
	i++
}}')
infDisk=$(du -sh ~ | awk 'BEGIN{OFS = ","} {print $2, $1}')

if [[ -f ~/log/metrics_agg_$TIMESTAMP1.log ]]
then
    chmod 600 ~/log/metrics_agg_$TIMESTAMP1.log
fi

echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > ~/log/metrics_$TIMESTAMP.log
echo "$infRam$infDisk" >> ~/log/metrics_$TIMESTAMP.log
echo "$infRam$infDisk" >> ~/log/ metrics_agg_$TIMESTAMP1.log

chmod 400 ~/log/metrics_$TIMESTAMP.log
chmod 400 ~/log/metrics_agg_$TIMESTAMP1.log

mkdir -p ~/log

TIMESTAMP=`date "+%Y%m%d%H"`

echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > ~/log/temp$TIMESTAMP.log

awk -F"," 'BEGIN{OFS=","; {i=1;
while (i <= 11) {
    if (i == 9) {i=i+2}
    min[i]=99999
    i++
}}}
END{{printf "minimum"}; {i=1;
while (i <= 11) {
    if (i == 9) {printf $10; i=i+2}
    if ($i < min[i]) {min[i]=$i}
    printf ","
    printf min[i]
    i++
}}}' ~/log/metrics_agg_$TIMESTAMP.log >> ~/log/temp$TIMESTAMP.log

awk -F"," 'BEGIN{OFS=","; {i=1;
while (i <= 11) {
    if (i == 9) {i=i+2}
    max[i]=0
    i++
}}}
END{{printf "\nmaximum"}; {i=1;
while (i <= 11) {
    if (i == 9) {printf $10; i+=2}
    if ($i > max[i]) {max[i]=$i}
    printf ","
    printf max[i]
    i++
}}}' ~/log/metrics_agg_$TIMESTAMP.log >> ~/log/temp$TIMESTAMP.log

awk -F"," 'BEGIN{OFS=","; {i=1;
while (i <= 11) {
    if (i == 9) {i=i+2}
    sum[i]=0
    i++
}}}
END{{printf "\naverage"}; {i=1;
while (i <= 11) {
    if (i == 9) {printf $10; i+=2}
    sum[i] += $i
    sum[i] /= NR
    printf ","
    printf sum[i]
    i++
}}{printf "\n"}}' ~/log/metrics_agg_$TIMESTAMP.log >> ~/log/temp$TIMESTAMP.log

chmod 700 ~/log/metrics_agg_$TIMESTAMP.log
cat temp$TIMESTAMP.log > ~/log/metrics_agg_$TIMESTAMP.log
chmod 400 ~/log/metrics_agg_$TIMESTAMP.log

rm ~/log/temp$TIMESTAMP.log

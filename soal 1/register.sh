#!/bin/bash

echo "What will your username be?"

while :
do
	read uname
	gr_uname=$(grep "$uname" ./users/user.txt | awk '{print $1;}')
	if [ "$uname" == "$gr_uname" ]
	then
		echo 'That username already exists, try again!'
		echo 'REGISTER: ERROR User already exists' >> log.txt
	else
		break
	fi
done

echo "Hello, $uname! Now, enter your password. (Your input will not be shown on screen)"
echo "Note : Password should be 8 characters long, have at minimum one uppercase letter and one lowercase letter, can be alphanumeric, and different from your username." 

while :
do
	read -r -s pwd
	[ ${#pwd} -ge 8 ] && [ $pwd != $uname ] && [[ "$pwd" =~ [[:upper:]] ]] && [[ "$pwd" =~ [[:lower:]] ]] && [[ $pwd =~ ^[a-zA-Z0-9]{3,20}$ ]] && break
	echo "Your password didn't meet the requirements, try again!"
done

echo "Register is a success!"
echo "LOGIN: INFO user $uname registered successfully" >> log.txt

mkdir ./users
echo "$uname $pwd" >> ./users/user.txt

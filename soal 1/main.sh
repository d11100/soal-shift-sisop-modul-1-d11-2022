#!/bin/bash
TIMESTAMP=`date "+%m-%d-%Y %H:%M:%S"`
TIMESTAMPS=`date "+%m-%d-%Y_"`
grpwd="";
echo "LOGIN FORM"
echo "Username : "

while :
do
    read uname
	gr_uname=$(grep "$uname" ./users/user.txt | awk '{print $1;}')
    if [ "$uname" == "$gr_uname" ]   
    then
        break
    else
        echo "Username does not exist"
    fi
done

gr_pwd=$(grep "$uname" ./users/user.txt | awk '{print $2;}')

echo "Password : "

while :
do
    read -r -s pwd
    if [ "$pwd" != "$gr_pwd" ]
    then
        echo "Wrong password"
        echo "$TIMESTAMP LOGIN: ERROR Failed login attempt on user $uname" >> log.txt
    else
        break
    fi
done

echo "Login success!"
echo "$TIMESTAMP LOGIN: INFO User $uname logged in" >> log.txt

echo "Welcome, $uname! How can I help you today?"

while :
do
	echo "Options :"
	echo "  1. dl x (Download the picture x number of times)"
	echo "  2. att (Check how many times you did the 'I forgor')"
	echo "  3. bye (Exit!)"
	read ans ans2

	declare -i ans3=0

	ans3=$((ans3 + ans2))

	if [ "$ans" == "dl" ]
		then	
			if [[ -f "$TIMESTAMPS$uname.zip" ]]
			then
				unzip -P $pwd $TIMESTAMPS$uname.zip
				rm $TIMESTAMPS$uname.zip
			else
				mkdir "$TIMESTAMPS$uname"
			fi
        		cd "$TIMESTAMPS$uname"
			declare -i count=1
			while [ $count -le $ans3 ]
        		do
				if [ $count -le 9 ]
				then
                			wget -O PIC_0$count https://loremflickr.com/320/240
                			count+=1
                			echo "$count"
				else
					wget -O PIC_$count https://loremflickr.com/320/240
					count+=1
					echo "$count"
        			fi
			done
			cd -
			zip --password $pwd -r $TIMESTAMPS$uname.zip $TIMESTAMPS$uname/
			rm -r $TIMESTAMPS$uname
	elif [ "$ans" == "att" ]
	then
        	declare -i count2=0

		count2=$(grep -c "LOGIN: ERROR Failed login attempt on user $uname" log.txt)

		count2+=$(grep -c  "LOGIN: INFO User $uname logged in" log.txt)

	echo "$count2"

	elif [ "$ans" == "bye" ]
	then
		echo "Goodbye, $uname! See you next time!"
		break
	else
        	echo "nothing there boss"
	fi
done

# soal-shift-sisop-modul-1-D11-2022


## Members
1. Benedictus Bimo C W - 5025201097
2. Hasna Lathifah Purbaningdyah - 5025201108
3. Muhammad Andi Akbar Ramadhan - 5025201264

## Soal 1

Pada suatu hari, Han dan teman-temannya diberikan tugas untuk mencari foto. Namun,
karena laptop teman-temannya rusak ternyata tidak bisa dipakai karena rusak, Han dengan
senang hati memperbolehkan teman-temannya untuk meminjam laptopnya. Untuk
mempermudah pekerjaan mereka, Han membuat sebuah program.

a. Han membuat sistem register pada script register.sh dan setiap user yang berhasil
didaftarkan disimpan di dalam file ./users/user.txt. Han juga membuat sistem login
yang dibuat di script main.sh

b. Demi menjaga keamanan, input password pada login dan register harus
tertutup/hidden dan password yang didaftarkan memiliki kriteria sebagai berikut :<br>
i. Minimal 8 karakter<br>
ii. Memiliki minimal 1 huruf kapital dan 1 huruf kecil<br>
iii. Alphanumeric<br>
iv. Tidak boleh sama dengan username

c. Setiap percobaan login dan register akan tercatat pada log.txt dengan format :
MM/DD/YY hh:mm:ss MESSAGE. Message pada log akan berbeda tergantung aksi
yang dilakukan user.<br>
i. Ketika mencoba register dengan username yang sudah terdaftar, maka
message pada log adalah REGISTER: ERROR User already exists<br>
ii. Ketika percobaan register berhasil, maka message pada log adalah REGISTER:
INFO User USERNAME registered successfully<br>
iii. Ketika user mencoba login namun passwordnya salah, maka message pada
log adalah LOGIN: ERROR Failed login attempt on user USERNAME<br>
iv. Ketika user berhasil login, maka message pada log adalah LOGIN: INFO User
USERNAME logged in

d. Setelah login, user dapat mengetikkan 2 command dengan dokumentasi sebagai
berikut :<br>
i. dl N ( N = Jumlah gambar yang akan didownload)
Untuk mendownload gambar dari https://loremflickr.com/320/240 dengan
jumlah sesuai dengan yang diinputkan oleh user. Hasil download akan
dimasukkan ke dalam folder dengan format nama
YYYY-MM-DD_USERNAME. Gambar-gambar yang didownload juga memiliki
format nama PIC_XX, dengan nomor yang berurutan (contoh : PIC_01,
PIC_02, dst. ). Setelah berhasil didownload semua, folder akan otomatis di
zip dengan format nama yang sama dengan folder dan dipassword sesuai
dengan password user tersebut. Apabila sudah terdapat file zip dengan
nama yang sama, maka file zip yang sudah ada di unzip terlebih dahulu,
barulah mulai ditambahkan gambar yang baru, kemudian folder di zip
kembali dengan password sesuai dengan user.<br>
ii. att
Menghitung jumlah percobaan login baik yang berhasil maupun tidak dari
user yang sedang login saat ini.

**Penyelesaian Soal**

**register.sh**

Input Username

```
while :
do
        read uname
        gr_uname=$(grep "$uname" ./users/user.txt | awk '{print $1;}')
        if [ "$uname" == "$gr_uname" ]
        then
                echo 'That username already exists, try again!'
                echo "$TIMESTAMP REGISTER: ERROR User already exists" >> log.txt
        else
                break
        fi
done
```

Input username akan dicek terlebih dahulu di user.txt dimana username dan password disimpan, jika sudah ada, user perlu menginput username yang berbeda.

Input Password
```
while :
do
        read -r -s pwd
        [ ${#pwd} -ge 8 ] && [ $pwd != $uname ] && [[ "$pwd" =~ [[:upper:]] ]] && [[ "$pwd" =~ [[:lower:]] ]] && [[ $pwd =~ ^[a-zA-Z0-9]{3,20}$ ]] && break
        echo "Your password didn't meet the requirements, try again!"
done
```

Input user untuk password akan tertutup dengan bantuan "-s"
Dibawah read terdapat beberapa syarat yang diperlukan untuk password, seperti :
Minimal 8 karakter, password tidak boleh sama dengan username, minimal terdapat 1 huruf besar dan 1 huruf kecil, serta password alphanumeric.

Penyimpanan username dan password
```
mkdir ./users
echo "$uname $pwd" >> ./users/user.txt
```
Username dan password yang telah diinput secara benar akan disimpan di ./users/user.txt menggunakan echo. Jika belum terdapat file-nya, akan dilakukan mkdir.

**main.sh**

Pengecekan username dan password
```
while :
do
    read uname
        gr_uname=$(grep "$uname" ./users/user.txt | awk '{print $1;}')
    if [ "$uname" == "$gr_uname" ]   
    then
        break
    else
        echo "Username does not exist"
    fi
done

gr_pwd=$(grep "$uname" ./users/user.txt | awk '{print $2;}')

echo "Password : "

while :
do
    read -r -s pwd
    if [ "$pwd" != "$gr_pwd" ]
    then
        echo "Wrong password"
        echo "$TIMESTAMP LOGIN: ERROR Failed login attempt on user $uname" >> log.txt
    else
        break
    fi
done
```
Terdapat "gr_pwd" untuk pengecekan password dari username yang telah diinput dengan menggunakan kombinasi grep dan awk.

Download dengan "dl x"
```
if [ "$ans" == "dl" ]
                then    
                        if [[ -f "$TIMESTAMPS$uname.zip" ]]
                        then
                                unzip -P $pwd $TIMESTAMPS$uname.zip
                                rm $TIMESTAMPS$uname.zip
                        else
                                mkdir "$TIMESTAMPS$uname"
                        fi
                        cd "$TIMESTAMPS$uname"
                        declare -i count=1
                        while [ $count -le $ans3 ]
                        do
                                if [ $count -le 9 ]
                                then
                                        wget -O PIC_0$count https://loremflickr.com/320/240
                                        count+=1
                                        echo "$count"
                                else
                                        wget -O PIC_$count https://loremflickr.com/320/240
                                        count+=1
                                        echo "$count"
                                fi
                        done
                        cd -
                        zip --password $pwd -r $TIMESTAMPS$uname.zip $TIMESTAMPS$uname/
                        rm -r $TIMESTAMPS$uname
```
Akan digunakan wget untuk download konten dari link yang tersedia. Terdapat juga pengecekan zip jika telah tersedia atau belum, jika belum akan dilakukan mkdir untuk di-zip nantinya. Konten yang telah di-download akan dimasukkan ke folder, setelah itu di-zip sesuai dengan format nama yang ada dan di-password dengan password user yang sedang login.

Cek Attempts dengan "att"
```
elif [ "$ans" == "att" ]
        then
                declare -i count2=0

                count2=$(grep -c "LOGIN: ERROR Failed login attempt on user $uname" log.txt)

                count2+=$(grep -c  "LOGIN: INFO User $uname logged in" log.txt)

        echo "$count2"
```
Akan di grep kedua kalimat yang berhubungan dengan login error dan logged in dengan username yang sedang login di sesi tersebut. grep -c  akan me-return suatu integer yang akan di echo nantinya.

**Demo Program**

![register.sh](images/register.jpg)
*Register.sh ketika dijalankan*

![main.sh](images/main.jpg)<br>
*Melakukan login di main.sh*

![main.sh](images/ongoing.jpg)
*Melakukan download di main.sh*

![file zip](images/downloaded.jpg)<br>
*File .zip yang telah didownload*

**Kendala Pengerjaan**
Soal 1 sangat straight-forward, kendala yang ditemui kebanyakan hanya karena baru pertama kali mengaplikasikan sesuatu berhubungan dengan shell scripting.

## Soal 2

Pada tanggal 22 Januari 2022, website https://daffa.info di hack oleh seseorang yang tidak
bertanggung jawab. Sehingga hari sabtu yang seharusnya hari libur menjadi berantakan.
Dapos langsung membuka log website dan menemukan banyak request yang berbahaya.
Bantulah Dapos untuk membaca log website https://daffa.info Buatlah sebuah script awk
bernama "soal2_forensic_dapos.sh" untuk melaksanakan tugas-tugas berikut:<br>
a. Buat folder terlebih dahulu bernama forensic_log_website_daffainfo_log.<br>
b. Dikarenakan serangan yang diluncurkan ke website https://daffa.info sangat banyak,
Dapos ingin tahu berapa rata-rata request per jam yang dikirimkan penyerang ke
website. Kemudian masukkan jumlah rata-ratanya ke dalam sebuah file bernama
ratarata.txt ke dalam folder yang sudah dibuat sebelumnya.<br>
c. Sepertinya penyerang ini menggunakan banyak IP saat melakukan serangan ke
website https://daffa.info, Dapos ingin menampilkan IP yang paling banyak
melakukan request ke server dan tampilkan berapa banyak request yang dikirimkan
dengan IP tersebut. Masukkan outputnya kedalam file baru bernama result.txt
kedalam folder yang sudah dibuat sebelumnya.<br>
d. Beberapa request ada yang menggunakan user-agent ada yang tidak. Dari banyaknya
request, berapa banyak requests yang menggunakan user-agent curl?
Kemudian masukkan berapa banyak requestnya kedalam file bernama result.txt yang
telah dibuat sebelumnya.<br>
e. Pada jam 2 pagi pada tanggal 23 terdapat serangan pada website, Dapos ingin
mencari tahu daftar IP yang mengakses website pada jam tersebut. Kemudian
masukkan daftar IP tersebut kedalam file bernama result.txt yang telah dibuat
sebelumnya.

Agar Dapos tidak bingung saat membaca hasilnya, formatnya akan dibuat seperti ini:

● File ratarata.txt<br>
```
Rata-rata serangan adalah sebanyak rata_rata requests per jam
```

● File result.txt<br>
```
IP yang paling banyak mengakses server adalah: ip_address sebanyak
jumlah_request requests
Ada jumlah_req_curl requests yang menggunakan curl sebagai user-agent
IP Address Jam 2 pagi
IP Address Jam 2 pagi
dst
```

* Gunakan AWK<br>
** Nanti semua file-file HASIL SAJA yang akan dimasukkan ke dalam folder
forensic_log_website_daffainfo_log

**Penyelesaian Soal**

**Poin a :** 
 ```
 # a
mkdir -p forensic_log_website_daffainfo_log
```
membuat folder bernama forensic_log_website_daffainfo_log.


**Poin b :**

Menghitung rata-rata request perjam lalu memasukkan hasilnya ke dalam file ratarata.txt
 ```
 # b
awk -F ":"  'NR>1 {ReqPerJam[$3]++}
END {
for (i in ReqPerJam) {
sum = sum + ReqPerJam[i]
TotalJam++
}
printf("Rata-rata serangan adalah sebanyak %d requests per jam", sum/TotalJam)
}' log_website_daffainfo.log>ratarata.txt
```
awk -F “:” berfungsi untuk membagi per line menjadi beberapa bagian yang dipisahkan dengan tanda “:”

Bagian ketiga ($3) dalam line menandakan jam.

Kemudian melakukan pengecekan ke seluruh kalimat/line dalam file log tersebut kecuali line pertama.

Menyimpan jumlah request per jam menggunakan array ReqPerJam[$3], kemudian menghitung total request dan total jam menggunakan for loop.

Menuliskan hasil perhitungan rata – rata request per jam yaitu total request dibagi total jam ke dalam file ratarata.txt


**Poin c :**

Menampilkan IP yang paling banyak melakukan request ke server dan menampilkan berapa banyak request yang dikirimkan dengan IP tersebut. Kemudian memasukkan outputnya kedalam file baru bernama result.txt kedalam folder yang sudah dibuat sebelumnya.
```
# c
awk -F ":" 'NR>1 {ipAdd[$1]++}
END{
max=0; IPmax="0"
for (ip in ipAdd) {
if(max<ipAdd[ip]) {
IPmax=ip
max=ipAdd[ip]}
}
printf("IP yang paling banyak mengakses server adalah: %s sebanyak %d request\n\n",IPmax, max)
}
' log_website_daffainfo.log > result.txt
```
Bagian pertama ($1) dalam line menandakan IP address.

Melakukan pengecekan ke seluruh kalimat/line dalam file log tersebut kecuali line pertama.

Menyimpan jumlah banyaknya IP yang melakukan request menggunakan array ipAdd[$1], kemudian menghitung total request masing-masing IP menggunakan for loop dan mencari jumlah request yang paling banyak

Menuliskan alamat IP yang melakukan request paling banyak (IPmax) beserta jumlah request nya (max) ke dalam file result.txt


**Poin d :**

Menghitung banyaknya request yang menggunakan user-agent curl
```
# d
awk -F ":" '/curl/ {++totalCurl}
END{printf("Ada %d requests yang menggunakan curl sebagai user agent\n\n", totalCurl)
}' log_website_daffainfo.log >> result.txt
```
Mengecek seluruh text dalam file yang mengandung kata “curl” menggunakan awk.

Kemudian menghitung jumlah kata “curl” yang muncul dan menyimpannya ke dalam variabel totalCurl.

Menuliskan hasilnya ke dalam file result.txt


**Poin e :**

Menuliskan daftar IP yang mengakses website pada jam 2 pagi
```
# e
awk -F ":" '$3 == "02" {print $1}' log_website_daffainfo.log | uniq | awk '{print}' >> result.txt 
```
Mengecek seluruh line dalam file log dimana $3 bernilai 02 yang menandakan jam 2 pagi
kemudian print $1 yang menandakan alamat IP ke dalam file result.txt, alamat IP yang kembar tidak akan di print lebih dari satu kali
```
mv ratarata.txt result.txt forensic_log_website_daffainfo_log
```
Memindahkan file ratarata.txt dan result.txt ke dalam folder forensic_log_website_daffainfo_log

**Demo Program**

![Folder forensic](images/forensic.jpg)<br>
*Folder forensic*

![File forensic](images/forensic1.jpg)<br>
*Hasil dari program*

![Rata-rata](images/ratarata.jpg)<br>
*Isi file ratarata.txt*

![Result](images/result.jpg)<br>
*Isi file result.txt*

**Kendala Pengerjaan**

Terdapat kesalahan pada jumlah rata-rata yang masih salah. Kesalahan ini baru kita ketahui pada saat demo.

## Soal 3

Ubay sangat suka dengan komputernya. Suatu saat komputernya crash secara tiba-tiba :(.
Tentu saja Ubay menggunakan linux. Akhirnya Ubay pergi ke tukang servis untuk
memperbaiki laptopnya. Setelah selesai servis, ternyata biaya servis sangatlah mahal
sehingga ia harus menggunakan dana kenakalannya untuk membayar biaya servis tersebut.
Menurut Mas Tukang Servis, laptop Ubay overload sehingga mengakibatkan crash pada
laptopnya. Karena tidak ingin hal serupa terulang, Ubay meminta kalian untuk membuat
suatu program monitoring resource yang tersedia pada komputer.

Buatlah program monitoring resource pada komputer kalian. Cukup monitoring ram dan
monitoring size suatu directory. Untuk ram gunakan command `free -m`. Untuk disk gunakan
command `du -sh <target_path>`. Catat semua metrics yang didapatkan dari hasil `free -m`.
Untuk hasil `du -sh <target_path>` catat size dari path directory tersebut. Untuk target_path
yang akan dimonitor adalah /home/{user}/.
a. Masukkan semua metrics ke dalam suatu file log bernama metrics_{YmdHms}.log.
{YmdHms} adalah waktu disaat file script bash kalian dijalankan. Misal dijalankan
pada 2022-01-31 15:00:00, maka file log yang akan tergenerate adalah
metrics_20220131150000.log.
b. Script untuk mencatat metrics diatas diharapkan dapat berjalan otomatis pada setiap
menit.
c. Kemudian, buat satu script untuk membuat agregasi file log ke satuan jam. Script
agregasi akan memiliki info dari file-file yang tergenerate tiap menit. Dalam hasil file
agregasi tersebut, terdapat nilai minimum, maximum, dan rata-rata dari tiap-tiap
metrics. File agregasi akan ditrigger untuk dijalankan setiap jam secara otomatis.
Berikut contoh nama file hasil agregasi metrics_agg_2022013115.log dengan format
metrics_agg_{YmdH}.log
d. Karena file log bersifat sensitif pastikan semua file log hanya dapat dibaca oleh user
pemilik file.

Note:
- nama file untuk script per menit adalah minute_log.sh
- nama file untuk script agregasi per jam adalah aggregate_minutes_to_hourly_log.sh
- semua file log terletak di /home/{user}/log

Berikut adalah contoh isi dari file metrics yang dijalankan tiap menit:
```
mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,sw
ap_free,path,path_size
15949,10067,308,588,5573,4974,2047,43,2004,/home/youruser/test/,74M
```

Berikut adalah contoh isi dari file aggregasi yang dijalankan tiap jam:
```
type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_us
ed,swap_free,path,path_size
minimum,15949,10067,223,588,5339,4626,2047,43,1995,/home/user/test/,50M
maximum,15949,10387,308,622,5573,4974,2047,52,2004,/home/user/test/,74M
average,15949,10227,265.5,605,5456,4800,2047,47.5,1999.5,/home/user/test/,62
```


**Penyelesaian Soal**

1. Secara umum, dilakukan pembagian 2 file, yaitu file minute_log.sh dan aggregate_minutes_to_hourly_log.sh. minute_log.sh akan menghasilkan 2 log, yaitu log setiap menit dan append log setiap jam. Pada aggregate_minutes_to_hourly_log.sh, log setiap jam yang sudah ada akan dibaca dan diproses untuk kemudian dituliskan kembali di file log setiap jam. Untuk mencegah terjadinya gangguan karena membaca dan menulis file log secara bersamaan, maka hasil dari aggregate_minutes_to_hourly_log.sh akan disimpan terlebih dahulu di file temp. Pada akhir aggregate_minutes_to_hourly_log.sh, isi dari file temp log akan di tuliskan kedalam file log setiap jam dan kemudian menghapus file temp log.

2.
```
mkdir -p ~/log

TIMESTAMP=`date "+%Y%m%d%H%M%S"`
TIMESTAMP1=`date "+%Y%m%d%H"`
infRam=$(free -m | awk 'BEGIN{OFS=","} NR!=1 {
i=2
while (i <= NF) {
	printf $i","
	i++
}}')
infDisk=$(du -sh ~ | awk 'BEGIN{OFS = ","} {print $2, $1}')

if [[ -f ~/log/metrics_agg_$TIMESTAMP1.log ]]
then
    chmod 600 ~/log/metrics_agg_$TIMESTAMP1.log
fi

echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > ~/log/metrics_$TIMESTAMP.log
echo "$infRam$infDisk" >> ~/log/metrics_$TIMESTAMP.log
echo "$infRam$infDisk" >> ~/log/ metrics_agg_$TIMESTAMP1.log

chmod 400 ~/log/metrics_$TIMESTAMP.log
chmod 400 ~/log/metrics_agg_$TIMESTAMP1.log
```
*Source minute_log.sh.*

Pada file minute_log.sh, digunakan TIMESTAMP dan TIMESTAMP1 untuk mendapatkan tanggal dan waktu yang akan digunakan sebagai nama dari file log. Untuk ram, digunakan perintah `free -m` dengan hasil dari perintah tersebut akan dipisahkan oleh tanda koma (,). Sedangkan untuk disk, digunakan perintah `du -sh` dengan pemisahan oleh tanda koma (,). Hasil resource dari ram dan disk tersebut akan append ke log setiap menit dan log setiap jam.

3. 
```
mkdir -p ~/log

TIMESTAMP=`date "+%Y%m%d%H"`

echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > ~/log/temp$TIMESTAMP.log

awk -F"," 'BEGIN{OFS=","; {i=1;
while (i <= 11) {
    if (i == 9) {i=i+2}
    min[i]=99999
    i++
}}}
END{{printf "minimum"}; {i=1;
while (i <= 11) {
    if (i == 9) {printf $10; i=i+2}
    if ($i < min[i]) {min[i]=$i}
    printf ","
    printf min[i]
    i++
}}}' ~/log/metrics_agg_$TIMESTAMP.log >> ~/log/temp$TIMESTAMP.log

awk -F"," 'BEGIN{OFS=","; {i=1;
while (i <= 11) {
    if (i == 9) {i=i+2}
    max[i]=0
    i++
}}}
END{{printf "\nmaximum"}; {i=1;
while (i <= 11) {
    if (i == 9) {printf $10; i+=2}
    if ($i > max[i]) {max[i]=$i}
    printf ","
    printf max[i]
    i++
}}}' ~/log/metrics_agg_$TIMESTAMP.log >> ~/log/temp$TIMESTAMP.log

awk -F"," 'BEGIN{OFS=","; {i=1;
while (i <= 11) {
    if (i == 9) {i=i+2}
    sum[i]=0
    i++
}}}
END{{printf "\naverage"}; {i=1;
while (i <= 11) {
    if (i == 9) {printf $10; i+=2}
    sum[i] += $i
    sum[i] /= NR
    printf ","
    printf sum[i]
    i++
}}{printf "\n"}}' ~/log/metrics_agg_$TIMESTAMP.log >> ~/log/temp$TIMESTAMP.log

chmod 700 ~/log/metrics_agg_$TIMESTAMP.log
cat temp$TIMESTAMP.log > ~/log/metrics_agg_$TIMESTAMP.log
chmod 400 ~/log/metrics_agg_$TIMESTAMP.log

rm ~/log/temp$TIMESTAMP.log
```
*Source aggregate_minutes_to_hourly_log.sh.*

Pada aggregate_minutes_to_hourly_log.sh, terdapat 3 perintah AWK masing-masing untuk minimum, maximum, dan average. Untuk minimum, setiap `$i` di dalam log akan diperiksa satu persatu yang memiliki nilai terkecil dan disimpan kedalam array. Untuk maximum, setiap `$i` di dalam log akan diperiksa satu persatu yang memiliki nilai terbesar dan disimpan kedalam array. Sedangkan untuk average, nilai dari `$i` setiap baris akan dijumlahkan dan kemudian dibagi dengan banyaknya log yang ada. Semua array hasil dari AWK tadi akan diappend kedalam file temp log terlebih dahulu. Pada akhirnya, file temp log akan melakukan write ke file log setiap jam. Kemudian file temp log akan dihapus.

**Demo Program**

![log program](images/output.jpg)
*Hasil log dari minute_log.sh dan aggregate_minutes_to_hourly_log.sh*

**Kendala Pengerjaan**

Kami merasa kesulitan pada saat melakukan perintah AWK pada aggregate_minutes_to_hourly_log.sh. Pada awalnya, terdapat hasil double di log setiap jam, namun ternyata hal itu dapat diatasi dengan menggunakan `BEGIN` dan `END`. Menurut saya, kendala ini terjadi karena saya baru menggunakan AWK beberapa hari sebelum praktikum, sehingga terdapat beberapa hal yang masih belum saya ketahui.

#!/bin/bash

# a
mkdir -p forensic_log_website_daffainfo_log

# b
awk -F ":"  'NR>1 {ReqPerJam[$3]++}
END {
for (i in ReqPerJam) {
sum = sum + ReqPerJam[i]
TotalJam++
}
printf("Rata-rata serangan adalah sebanyak %d requests per jam", sum/TotalJam)
}' log_website_daffainfo.log>ratarata.txt

# c
awk -F ":" 'NR>1 {ipAdd[$1]++}
END{
max=0; IPmax="0"
for (ip in ipAdd) {
if(max<ipAdd[ip]) {
IPmax=ip
max=ipAdd[ip]}
}
printf("IP yang paling banyak mengakses server adalah: %s sebanyak %d request\n\n",IPmax, max)
}
' log_website_daffainfo.log > result.txt

# d
awk -F ":" '/curl/ {++totalCurl}
END{printf("Ada %d requests yang menggunakan curl sebagai user agent\n\n", totalCurl)
}' log_website_daffainfo.log >> result.txt

# e
awk -F ":" '$3 == "02" {print $1}' log_website_daffainfo.log | uniq | awk '{print}' >> result.txt 

mv ratarata.txt result.txt forensic_log_website_daffainfo_log
